import React, { Component } from 'react';
import Item from './Item';


class List extends Component {
  constructor(props){
    super(props);

    this.state = {
    };
    
  }

  render(){
    const items = this.props.items;
    const elmItem = items.map((item, index)=>{
      return(
        <Item 
          onClickDelete={this.props.onClickDelete}
          onClickEdit={this.props.onClickEdit}
          key={index} 
          item={item} 
          index ={index} />
      );
    });

    return (
      <div className="panel panel-success">
              <h3 className="panel-heading">List Task</h3>

                <table className="table table-hover text-center">
                  <thead>
                    <tr>
                      <th scope="col" className="text-center" style={{width: '10%'}}>#</th>
                      <th scope="col" className="text-center" style={{width: '50%'}}>Task</th>
                      <th scope="col" className="text-center" style={{width: '20%'}}>Level</th>
                      <th scope="col" className="text-center" style={{width: '20%'}}>Action</th>
                    </tr>
                  </thead>
                  <tbody>{elmItem}</tbody>
                </table>
            </div>
      );
    }
}


export default List;
