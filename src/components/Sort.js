import React, { Component } from 'react';


class Sort extends Component {
  constructor(props){
    super(props);

    this.state = {
    };

    this.handleSort = this.handleSort.bind(this);
  }

  handleSort(orderBy, orderDir){
    console.log("handleSort: ", orderBy + " - " , orderDir);
    this.props.onClickSort(orderBy, orderDir);
  }

  render(){
    let {orderBy}     = this.props;
    let {orderDir}   = this.props;

    let strSort = orderBy + " - " + orderDir;

    return (
      <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3 ">
        <div className="dropdown">
          <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" >
            Sort by <span className="caret" />
          </button>
          <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><a onClick={()=>this.handleSort('NAME', 'ASC')} className ="dropdown-item"  href='/#'role="button">NAME ACS</a></li>
            <li><a onClick={()=>this.handleSort('NAME', 'DESC')} className ="dropdown-item" href='/#' role="button">NAME DESC</a></li>
            <li role="separator" className ="divider" />
            <li><a onClick={()=>this.handleSort('LEVEL', 'ASC')} cllevel ="dropdown-item" href='/#' role="button">LEVEL ACS</a></li>
            <li><a onClick={()=>this.handleSort('LEVEL', 'DESC')} clalevelme ="dropdown-item" href='/#' role="button">LEVEL DESC</a></li>
          </ul>
          <span className="btn btn-success btn-medium">{strSort}</span>
        </div>
      </div>
      );
    }
}


export default Sort;
