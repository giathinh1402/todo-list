import React, { Component } from 'react';
// import { ClientHttpSession } from 'http';


class Form extends Component {
  constructor(props){
    super(props);

    this.state = {
      task_id: '',
      task_name: '',
      task_level: 0
    };

    

    this.handleCancel = this.handleCancel.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    
  }

  componentWillMount(){
    let item  = this.props.itemSelected;

    if(item !== null ){
      this.setState({
        task_id: item.id,
        task_name: item.name,
        task_level: item.level,
      });
    }
  }

  componentWillReceiveProps(nextProps){
    let item  = nextProps.itemSelected;
    if(nextProps !== null ){
      this.setState({
        task_id: item.id,
        task_name: item.name,
        task_level: item.level,
      });
    }
  }

  handleChange(event){
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event){

    let item ={
      name: this.state.task_name,
      level: this.state.task_level,
      id: this.state.task_id,
    };

    this.props.onClickSubmit(item);
    event.preventDefault();
  }

  handleCancel(){
    this.props.onClickCancel();
  }

  render(){
    return ( 
    <div className="container">
      <div className="row col-md-12">
        <div className="col-md-offset-7 col-md-6">
          <form onSubmit={this.handleSubmit} className ="form-inline">

            <div className="form-group">
              <label className="sr-only" >Label</label>
              <input value={this.state.task_name} onChange={this.handleChange} name="task_name" type="text" id="task_name" className="form-control" placeholder="Task Name"  />
            </div>

            <div className="form-group">
              <div className="sr-only" >Label</div>
                <select value={this.state.task_level} onChange={this.handleChange} name="task_level" id="inputDs" className="form-control" required="required">
                  <option value={0}>Small</option>
                  <option value={1}>Medium</option>
                  <option value={2}>High</option>
                </select>
            </div>
            
            <button className="btn btn-primary" type="submit">Submit</button>
            <button onClick={this.handleCancel} className="btn btn-danger" type="button">Cancel</button>
          </form>
        </div>
      </div>
    </div>
      );
    }
  }


export default Form;
