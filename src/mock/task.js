const uuidv4 = require('uuid/v4');
let items = [
    {
      id: uuidv4(),
      name: "Abc Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      level: 0 // 0 small, 1 medium, 2 hight 
    },

    {
      id: uuidv4(),
      name: "Def Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      level: 1 // 0 small, 1 medium, 2 hight 
    },

    {
      id: uuidv4(),
      name: "Ghj Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      level: 2 // 0 small, 1 medium, 2 hight 
    }
];


export default items;