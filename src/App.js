import React, { Component } from 'react';
import Title from './components/Title';
import Control from './components/Control';
import Form from './components/Form';
import List from './components/List';
import {orderBy as funcOrderBy, remove, reject} from 'lodash';
import task from './mock/task';

const uuidv4 = require('uuid/v4');

class App extends Component {
  constructor(props){
    super(props);

    this.state = {
      items       : task,
      isShowForm  : false,
      strSearch   : '',
      orderBy     : 'name',
      orderDir    : 'asc',
      itemSelected: null
    };

    this.handleToggleForm = this.handleToggleForm.bind(this);
    this.closeForm        = this.closeForm.bind(this);
    this.handleSearch     = this.handleSearch.bind(this);
    this.handleSort       = this.handleSort.bind(this);
    this.handleDelete     = this.handleDelete.bind(this);
    this.handleSubmit     = this.handleSubmit.bind(this);
    this.handleEdit       = this.handleEdit.bind(this);
  }

  handleToggleForm(){
    this.setState ({
      isShowForm: !this.state.isShowForm,
      itemSelected: null
    });
  }

  handleSearch(value){
    this.setState ({
      strSearch: value
    });
  }

  handleSort(orderBy, orderDir){
    this.setState({
      orderBy: orderBy,
      orderDir: orderDir
    });
  }

  handleDelete(id){
    console.log(id);
    let items = this.state.items
    remove(items, (item)=>{
      return item.id === id;
    });
    this.setState({
      items: items
    });
  }

  handleEdit(item){
    this.setState({
      itemSelected: item,
      isShowForm: true
    });
  }

  handleSubmit(item){  
    
    let {items} = this.state;
    let id = null;

    if(item.id !== ''){
      items = reject(items, { id: item.id});
      id = item.id;
    } else {
      id    = uuidv4();
    }

    items.push({
      id    : id,
      name  : item.name,
      level : +item.level
    })

    this.setState({
      items: items,
      isShowForm: false
    });
  }

  closeForm(){
    this.setState({
      isShowForm: false
    });
  }
  
  render(){
    console.log("strSearch: ", this.state.strSearch);
    let itemsOrigin = this.state.items;
    let items       =[];
    let elmForm     = true;
    let {orderBy, orderDir, isShowForm}   = this.state;

    const search    = this.state.strSearch;
    // console.log(orderBy + "-" + orderDir );

    items = funcOrderBy(items, [orderBy], [orderDir]);

    if(search.length >0){
      itemsOrigin.forEach((item) => {
        if(item.name.toLowerCase().indexOf(search) !== -1){
         items.push(item);
        }
      });
    }else{
      items = itemsOrigin;
    }

    if(isShowForm){
      elmForm = <Form itemSelected={this.state.itemSelected} onClickSubmit={this.handleSubmit} onClickCancel={this.closeForm}/>;
    }

    return (
      <div className="container">
        {/*Title */}
          < Title />
        {/*End Title */}

        {/* Control(Search + Sort + Add)*/}
          < Control
            onClickSearchGo={this.handleSearch}
            strSearch={this.props.strSearch}
            onClickAdd = {this.handleToggleForm}
            isShowForm={isShowForm}
            onClickSort = {this.handleSort}
            orderBy= {orderBy}
            orderDir={orderDir}
            />
        {/*End control*/}

        {/*FORM START*/}
          {elmForm }
        {/*FORM  END*/}

        {/*List Start*/}
          <List
            onClickDelete = {this.handleDelete}
            onClickEdit = {this.handleEdit}
            items={items} 
          />
        {/*List End*/} 
      </div>
      );
    }
}


export default App;
